function guardarEnLocalStorage() {

  var clave = document.getElementById('txtClave').value;
  var valor = document.getElementById('txtValor').value;
  console.log(clave, valor);
  localStorage.setItem(clave,valor);

  var objetoJson = {
  "nombre":"Abraham",
  "apellido":"Valle",
  "pais":"Mexico",
  "ciudad":"CDMX"
};

localStorage.setItem("json", JSON.stringify(objetoJson));


}

function leerDeLocalStorage(){
  var clave = document.getElementById('txtClave').value;

  var valor = localStorage.getItem(clave);
  document.getElementById('spanValor').innerText= valor;
  var datosUsuario = JSON.parse(localStorage.getItem("json"));
  console.log(datosUsuario.nombre);
}
